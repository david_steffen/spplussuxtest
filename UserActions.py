import json
from tkinter import *

class UserActions:
    def __init__(self, driver, base_url, cat):
        self.driver = driver
        self.base_url = base_url
        self.cat = cat
        self.admin_creds = None
        self.user_creds = self.get_user_creds()
        self.driver.get(self.base_url)
        self.close_modal()
        self.entryWidget = None


    def header_login(self):
       # WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(self.driver.find_element_by_id('MemberLoginForm_LoginForm')))
        scope = self.driver.find_element_by_id('MemberLoginForm_LoginForm')
        scope.find_element_by_id('MemberLoginForm_LoginForm_Email').send_keys(self.user_creds['email'])
        scope.find_element_by_id('MemberLoginForm_LoginForm_Password').send_keys(self.user_creds['password'])
        scope.find_element_by_id('MemberLoginForm_LoginForm_action_dologin').submit()

    def admin_login(self):
        scope = self.driver.find_element_by_id('MemberLoginForm_LoginForm')
        scope.find_element_by_id('MemberLoginForm_LoginForm_Email').send_keys(self.admin_creds['email'])
        scope.find_element_by_id('MemberLoginForm_LoginForm_Password').send_keys(self.admin_creds['password'])
        scope.find_element_by_id('MemberLoginForm_LoginForm_action_dologin').submit()

    def get_user_creds(self):
        json_data = open("user_details.json")
        #json_data = open('json_data')
        data = json.load(json_data)
        self.admin_creds = data['admin_creds']
        json_data.close()
        return {
            'agency': data['agency_creds'],
            'staff': data['staff_creds'],
            }.get(self.cat, data['agency_creds'])

    def basic_register(self, rel_path):
        self.driver.get(self.base_url + rel_path)
        scope = self.driver.find_element_by_id('Form_RegForm')
        scope.find_element_by_id('Form_RegForm_FirstName').send_keys(self.user_creds['first_name'])
        scope.find_element_by_id('Form_RegForm_Surname').send_keys(self.user_creds['last_name'])
        scope.find_element_by_id('Form_RegForm_Email').send_keys(self.user_creds['email'])
        scope.find_element_by_id('Form_RegForm_Latitude').send_keys(self.user_creds['email'])
        scope.find_element_by_id('Form_RegForm_MobileTelephoneNumber').send_keys(self.user_creds['mobile'])
        scope.find_element_by_id('Form_RegForm_HomeTelephoneNumber').send_keys(self.user_creds['tel'])
        scope.find_element_by_id('Form_RegForm_Postcode').send_keys(self.user_creds['postcode'])
        scope.find_element_by_id('Form_RegForm_HouseNumber').send_keys(self.user_creds['building_no'])
        scope.find_element_by_id("AddressLookup").click()
        return scope

    def authenticate_email(self):
        root = Tk()
        root.title("Email validation link")
        root["padx"] = 40
        root["pady"] = 20

         # Create a text frame to hold the text Label and the Entry widget
        textFrame = Frame(root)

         #Create a Label in textFrame
        entryLabel = Label(textFrame)
        entryLabel["text"] = "Enter the link:"
        entryLabel.pack(side=LEFT)

         # Create an Entry Widget in textFrame
        self.entryWidget = Entry(textFrame)
        self.entryWidget["width"] = 50
        self.entryWidget.pack(side=LEFT)

        textFrame.pack()

        button = Button(root, text="Submit", command=self.getUrl)
        button.pack()

    def vet_email(self):
        self.admin_login()
        self.driver.get(self.base_url + "/admin/security/")
        scope = self.driver.find_element_by_id('MemberList')
        allAnchors = scope.find_elements_by_tag_name("a")
        for anchor in allAnchors:
            link = anchor.get_attribute("href")
            link = link.replace("%40", "@")
            if self.user_creds['email'] in link:
                anchor.click()
                break
        self.driver.find_element_by_link_text("Logout")

    def getUrl(self):
        input = self.entryWidget.get()
        self.driver.get(input)
        self.vet_email()


    def close_modal(self):
        self.driver.find_element_by_class_name('closeModal').click()