from UserActions import UserActions
from selenium.webdriver.support.ui import Select

class StaffActions(UserActions):

    def login(self):
        self.driver.get(self.base_url + "logistical-planner/")
        print (self.driver.title)

    def register(self):
        rel_path = "/staff/register/"
        scope = self.basic_register(rel_path)
        Select(scope.find_element_by_id("Form_RegForm_DateOfBirth-day")) \
            .select_by_visible_text(self.user_creds['day'])
        Select(scope.find_element_by_id("Form_RegForm_DateOfBirth-month")) \
            .select_by_visible_text(self.user_creds['month'])
        Select(scope.find_element_by_id("Form_RegForm_DateOfBirth-year")) \
            .select_by_visible_text(self.user_creds['year'])
        Select(scope.find_element_by_id("crafty_postcode_lookup_result_option1"))\
            .select_by_visible_text(self.user_creds['house_name'])
        scope.find_element_by_id("Form_RegForm_AcceptedTermsAndConditions").click()
        scope.find_element_by_id("Form_RegForm_action_doRegistration").click()

    def run(self):
        #self.register()
        #self.authenticate_email()
        self.header_login()