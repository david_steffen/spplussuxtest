from UserActions import UserActions

from selenium.webdriver.support.ui import Select

class AgencyActions(UserActions):

    def login(self):
        self.driver.get(self.base_url + "logistical-planner/")
        print (self.driver.title)

    def register(self):
        rel_path = "/agency/register/"
        scope = self.basic_register(rel_path)
        #select = scope.find_elements_by_id("crafty_postcode_lookup_result_option1")
        select = Select(scope.find_element_by_id("crafty_postcode_lookup_result_option1"))
        select.select_by_visible_text(self.user_creds['company_name'])
        scope.find_element_by_id("Form_RegForm_AcceptedTermsAndConditions").click()
        scope.find_element_by_id("Form_RegForm_action_doRegistration").click()

    def run(self):
        #self.register()
        #self.authenticate_email()
        self.header_login()
